package tests;

import com.google.common.collect.ImmutableMap;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;

public class BaseClass {

    AppiumDriver<MobileElement> driver;

    @BeforeTest
    public void init() {
        try {
            DesiredCapabilities caps = new DesiredCapabilities();

            caps.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
            caps.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8.1");
            caps.setCapability(MobileCapabilityType.DEVICE_NAME, "SM A515F");
            caps.setCapability(MobileCapabilityType.UDID, "RR8NB05DREH");
            caps.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
            caps.setCapability("chromeOptions", ImmutableMap.of("w3c", false));
            caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "60");

            URL url = new URL("http://127.0.0.1:4723/wd/hub");

            driver = new AppiumDriver<MobileElement>(url, caps);
        } catch (Exception exp) {
            System.out.println("Cause is : "+exp.getCause());
            System.out.println("Messages is : "+exp.getMessage());
        }
    }

    @AfterTest
    public void destroy() {
        driver.close();
        driver.quit();
    }
}
